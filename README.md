## Maja Francetic - CV Website

A webpage that represents a website CV, with the About Me, Education and Skills,
Projects and Contact sections.

The website is created using HTML, CSS and Bulma CSS framework.

The page is served under: https://mfrancetic.gitlab.io/cv-website